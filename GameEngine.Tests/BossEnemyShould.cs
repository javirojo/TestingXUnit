﻿

using Xunit;
using Xunit.Abstractions;

namespace GameEngine.Tests
{
    public class BossEnemyShould
    {
        private readonly ITestOutputHelper _output;

        public BossEnemyShould(ITestOutputHelper output)
        {
            _output = output;
        }

        #region Number
        
        [Fact]
        [Trait("Category", "Enemy")]
        public void HaveCorrectSpecialAttackPower()
        {
            _output.WriteLine("Crating Boss Enemy");

            BossEnemy sut = new BossEnemy();

            Assert.Equal(166.667, sut.SpecialAttackPower, 3);
             
        }
        
        #endregion Number
    }
}
