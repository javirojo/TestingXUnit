using System;
using Xunit;
using Xunit.Abstractions;

namespace GameEngine.Tests
{
    public class PlayerCharacterShould : IDisposable
    {
        private readonly ITestOutputHelper _output;

        private readonly PlayerCharacter _sut;

        public PlayerCharacterShould(ITestOutputHelper output)
        {
            _output = output;
            _sut = new PlayerCharacter();

            _output.WriteLine("Creating new PlayerCharacter");
        }

        public void Dispose()
        {
            _output.WriteLine($"Disposing PlayerCharacter {_sut.FullName}");
        }


        #region Bool

        [Fact]
        public void BeInexperienceWhenNew()
        {       
            Assert.True(_sut.IsNoob);
        }

        #endregion Bool

        #region String

        [Fact]
        public void CalculateFullName()
        {
            _sut.FirstName = "Jhon";
            _sut.LastName = "Doe";

            Assert.Equal("Jhon Doe", _sut.FullName);
        }

        [Fact]
        public void CalculateFullNameIgnoringCase()
        {
            _sut.FirstName = "JHON";
            _sut.LastName = "DOE";

            Assert.Equal("Jhon Doe", _sut.FullName, ignoreCase: true);
        }

        [Fact]
        public void HaveFullNameStartingWithName()
        {
            _sut.FirstName = "Jhon";
            _sut.LastName = "Doe";

            Assert.StartsWith("Jhon", _sut.FullName);
        }

        [Fact]
        public void HaveFullNameEndingWithLastName()
        {
            _sut.FirstName = "Jhon";
            _sut.LastName = "Doe";

            Assert.EndsWith("Doe", _sut.FullName);
        }

        [Fact(Skip ="No need to run")]
        public void HaveFullName_SubtringExample()
        {
            _sut.FirstName = "Jhon";
            _sut.LastName = "Doe";

            Assert.Contains("on Do", _sut.FullName);
        }

        [Fact]
        public void CalculateFullNameWithTitleCase()
        {
            _sut.FirstName = "Jhon";
            _sut.LastName = "Doe";

            Assert.Matches("[A-Z]{1}[a-z]+ [A-Z]{1}[a-z]+", _sut.FullName);
        }

        #endregion String

        #region Number

        [Fact]
        public void StartWithDefaultHealth()
        {
            Assert.Equal(100, _sut.Health);
        }

        [Fact]
        public void StartWithDefaultHealth_NotEqualExample()
        {
            Assert.NotEqual(0, _sut.Health);
        }

        [Fact]
        public void IncreaseHealthAfterSleeping()
        {
            _sut.Sleep();

            Assert.True(_sut.Health >= 101 && _sut.Health <= 200);
            Assert.InRange<int>(_sut.Health, 101, 200);
        }

        #endregion Number

        #region Null 

        [Fact]
        public void NotHaveNickNameByDefault()
        {
            Assert.Null(_sut.Nickname);
        }

        #endregion Null

        #region Collections

        [Fact]
        public void HaveALongBow()
        {
            Assert.Contains("Long Bow", _sut.Weapons);
        }

        [Fact]
        public void NotHaveAStaffOfWonder()
        {
            Assert.DoesNotContain("Staff Of Wonder", _sut.Weapons);
        }

        [Fact]
        public void HaveAtLeastOneKindOfSword()
        {
            Assert.Contains(_sut.Weapons, weapon => weapon.Contains("Sword"));
        }

        [Fact]
        public void HaveAllExpectedWeapons()
        {
            var expectedWeapons = new[]
            {
                "Long Bow",
                "Short Bow",
                "Short Sword"
            };

            Assert.Equal(expectedWeapons, _sut.Weapons);
        }

        [Fact]
        public void HaveNoEmptyOrDefaultWeapons()
        {
            Assert.All(_sut.Weapons, weapon => Assert.False(string.IsNullOrWhiteSpace(weapon)));
        }

        #endregion Collections

        #region Events
        
        [Fact]
        public void RaiseSleepEvent()
        {
            Assert.Raises<EventArgs>(
                handler => _sut.PlayerSlept += handler,
                handler => _sut.PlayerSlept -= handler,
                () => _sut.Sleep());
        }

        [Fact]
        public void RaisePropertyChangeEvent()
        {
            Assert.PropertyChanged(_sut, "Health", () => _sut.TakeDamage(10));
        }

        #endregion Events

        [Theory]
        [InlineData(0,100)]
        [InlineData(25,75)]
        [InlineData(50,50)]
        [InlineData(75,25)]
        [InlineData(101,1)]
        public void TakeDamage(int damage, int expectedHealth)
        {
            _sut.TakeDamage(damage);

            Assert.Equal(expectedHealth, _sut.Health);
        }

        [Theory]
        [MemberData(nameof(InternalHealthDamageTestData.TestData), MemberType = typeof(InternalHealthDamageTestData))]
        public void TakeDamage_InternalDataExample(int damage, int expectedHealth)
        {
            _sut.TakeDamage(damage);

            Assert.Equal(expectedHealth, _sut.Health);
        }

        [Theory]
        [MemberData(nameof(ExternalHealthDamageTestData.TestData), MemberType = typeof(ExternalHealthDamageTestData))]
        public void TakeDamage_ExternalDataExample(int damage, int expectedHealth)
        {
            _sut.TakeDamage(damage);

            Assert.Equal(expectedHealth, _sut.Health);
        }

        [Theory]
        [HealthDamageData]
        public void TakeDamage_DataAttributeExample(int damage, int expectedHealth)
        {
            _sut.TakeDamage(damage);

            Assert.Equal(expectedHealth, _sut.Health);
        }
    }
}
